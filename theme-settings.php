<?php

function multi_purpose_form_system_theme_settings_alter(&$form, &$form_state) {

  $form['#attached']['library'][] = 'multi_purpose/theme-settings';

  $form['multi_purpose'] = [
    '#type'       => 'vertical_tabs',
    '#title'      => '<h3 class="settings-form-title">' . t('') . '</h3>',
    '#default_tab' => 'general',
  ];
  
  /**
   * Main Tabs.
   */
  $form['hero_banner'] = [
    '#type'  => 'details',
    '#title' => t('Hero Banner'),
    '#description' => t('<h3>You can find information about the hero Banner here...</h3><br>'),
    '#group' => 'multi_purpose',
  ];

  $form['hero_banner']['hero_banner_tab'] = [
    '#type'  => 'vertical_tabs',
  ];


  $form['banner_left'] = [
    '#type'  => 'details',
    '#title' => t('Banner Left'),
    '#description' => t('<h3>You can find information about the Banner left here...</h3><br>'),
    '#group' => 'multi_purpose',
  ];

  $form['banner_left']['banner_left_tab'] = [
    '#type'  => 'vertical_tabs',
  ];


  $form['banner_right'] = [
    '#type'  => 'details',
    '#title' => t('Banner Right'),
    '#description' => t('<h3>You can find information about the Banner right here...</h3><br>'),
    '#group' => 'multi_purpose',
  ];

  $form['banner_right']['banner_right_tab'] = [
    '#type'  => 'vertical_tabs',
  ];


  $form['Products'] = [
    '#type'  => 'details',
    '#title' => t('Material Product'),
    '#description' => t('<h3>You can enter the information about material product here...</h3><br>'),
    '#group' => 'multi_purpose',
  ];

  $form['Products']['product_tab'] = [
    '#type'  => 'vertical_tabs',
  ];


  $form['About'] = [
    '#type'  => 'details',
    '#title' => t('About Us'),
    '#description' => t('<h3>You can enter the information for about us section here...</h3><br>'),
    '#group' => 'multi_purpose',
  ];

  $form['About']['About_us'] = [
    '#type'  => 'vertical_tabs',
  ];


  $form['video_banner'] = [
    '#type'  => 'details',
    '#title' => t('Video Banner'),
    '#description' => t('<h3>You can enter the information for Video Banner section here...</h3><br>'),
    '#group' => 'multi_purpose',
  ];

  $form['video_banner']['video_banner_tab'] = [
    '#type'  => 'vertical_tabs',
  ];


  $form['testimonial'] = [
    '#type'  => 'details',
    '#title' => t('Testimonials'),
    '#description' => t('<h5>The section Contain information for Testimonial here you can add testimonial data...</h5><br>'),
    '#group' => 'multi_purpose',
  ];

  $form['testimonial']['testimonial_tab'] = [
    '#type'  => 'vertical_tabs',
  ];


  $form['blog'] = [
    '#type'  => 'details',
    '#title' => t('Blog'),
    '#description' => t('<h5>The section Contain information for Blog section...</h5><br>'),
    '#group' => 'multi_purpose',
  ];

  $form['blog']['blog_tab'] = [
    '#type'  => 'vertical_tabs',
  ];

  $form['footer_logo'] = [
    '#type'  => 'details',
    '#title' => t('Footer Logo'),
    '#description' => t('<h5>The section Contain Images for Footer Logo region...</h5><br>'),
    '#group' => 'multi_purpose',
  ];

  $form['footer_logo']['footer_logo_tab'] = [
    '#type'  => 'vertical_tabs',
  ];

  $form['footer_description'] = [
    '#type'  => 'details',
    '#title' => t('Footer Description'),
    '#description' => t('<h5>The section Contain information for Footer section Description...</h5><br>'),
    '#group' => 'multi_purpose',
  ];

  $form['footer_description']['footer_desc_tab'] = [
    '#type'  => 'vertical_tabs',
  ];

  /**
   * Hero Banner
   */
  $form['hero_banner']['hero_banner_image'] = [
    '#type' => 'managed_file',
    '#title' => t('Upload image for hero banner.'),
    '#default_value' => theme_get_setting('hero_banner_image'),
    '#upload_location' => 'public://theme/herobanner/',
    '#upload_validators' => [
      'file_validate_extensions' => ['jpg jpeg png webp'],
    ],
  ];

  $form['hero_banner']['hero_banner_subheading'] = [
    '#type'        => 'textfield',
    '#title'       => t('Sub Heading'),
    '#default_value' => theme_get_setting('hero_banner_subheading'),
    '#description'   => t('You can enter the sub heading for hero banner section.')
  ];

  $form['hero_banner']['hero_banner_heading'] = [
    '#type'        => 'textfield',
    '#title'       => t('Heading'),
    '#default_value' => theme_get_setting('hero_banner_heading'),
    '#description'   => t('You can enter the heading for hero banner section.')
  ];

  $form['hero_banner']['hero_banner_desc'] = [
    '#type'        => 'textarea',
    '#title'       => t('Description'),
    '#default_value' => theme_get_setting('hero_banner_desc'),
    '#description'   => t('You can enter the Description for hero banner section. you can enter the character upto 137')
  ];

  $form['hero_banner']['hero_btnlink'] = [
    '#type'        => 'fieldset',
    '#title'       => t('Button Link'),
    '#description'   => t('')
  ];

  $form['hero_banner']['hero_btnlink']['button_text'] = [
    '#type'        => 'textfield',
    '#title'       => t('Button Text'),
    '#default_value' => theme_get_setting('button_text'),
    '#description'   => t('You can enter the text for button..')
  ];

  $form['hero_banner']['hero_btnlink']['button_url'] = [
    '#type'        => 'textfield',
    '#title'       => t('Button url'),
    '#default_value' => theme_get_setting('button_url'),
    '#description'   => t('You can enter an external URL such as http://example.com.')
  ];

  /**
   * Banner Left
   */
  $form['banner_left']['banner_left_image'] = [
    '#type' => 'managed_file',
    '#title' => t('Upload image for banner left.'),
    '#default_value' => theme_get_setting('banner_left_image'),
    '#upload_location' => 'public://theme/Banner-left/',
    '#upload_validators' => [
      'file_validate_extensions' => ['jpg jpeg png webp'],
    ],
  ];

  $form['banner_left']['banner_left_heading'] = [
    '#type'        => 'textfield',
    '#title'       => t('Heading'),
    '#default_value' => theme_get_setting('banner_left_heading'),
    '#description'   => t('You can enter the heading for banner left section.')
  ];

  $form['banner_left']['banner_left_btnlink'] = [
    '#type'        => 'fieldset',
    '#title'       => t('Button Link'),
    '#description'   => t('')
  ];

  $form['banner_left']['banner_left_btnlink']['banner_btntext'] = [
    '#type'        => 'textfield',
    '#title'       => t('Button Text'),
    '#default_value' => theme_get_setting('banner_btntext'),
    '#description'   => t('You can enter the text for button..')
  ];

  $form['banner_left']['banner_left_btnlink']['banner_btnurl'] = [
    '#type'        => 'textfield',
    '#title'       => t('Button url'),
    '#default_value' => theme_get_setting('banner_btnurl'),
    '#description'   => t('You can enter an external URL such as http://example.com.')
  ];

  /**
   * Banner Right
   */
  $form['banner_right']['banner_right_image'] = [
    '#type' => 'managed_file',
    '#title' => t('Upload image for banner right.'),
    '#default_value' => theme_get_setting('banner_right_image'),
    '#upload_location' => 'public://theme/Banner-right/',
    '#upload_validators' => [
      'file_validate_extensions' => ['jpg jpeg png webp'],
    ],
  ];

  $form['banner_right']['banner_right_heading'] = [
    '#type'        => 'textfield',
    '#title'       => t('Heading'),
    '#default_value' => theme_get_setting('banner_right_heading'),
    '#description'   => t('You can enter the heading for banner right section.')
  ];

  $form['banner_right']['banner_right_btnlink'] = [
    '#type'        => 'fieldset',
    '#title'       => t('Button Link'),
    '#description'   => t('')
  ];

  $form['banner_right']['banner_right_btnlink']['banner_right_btntext'] = [
    '#type'        => 'textfield',
    '#title'       => t('Button Text'),
    '#default_value' => theme_get_setting('banner_right_btntext'),
    '#description'   => t('You can enter the text for button..')
  ];

  $form['banner_right']['banner_right_btnlink']['banner_right_btnurl'] = [
    '#type'        => 'textfield',
    '#title'       => t('Button url'),
    '#default_value' => theme_get_setting('banner_right_btnurl'),
    '#description'   => t('You can enter an external URL such as http://example.com.')
  ];

  /**
   * cards data  1
   */
  $form['Products']['card_one'] = [
    '#type'        => 'details',
    '#title'       => t('Card one'),
    '#description'   => t(''),
    '#group' => 'product_tab',
  ];

  $form['Products']['card_one']['details'] = [
    '#type'        => 'fieldset',
    '#title'       => t('Card Details'),
    '#description'   => t('')
  ];

  $form['Products']['card_one']['details']['product_image_1'] = [
    '#type' => 'managed_file',
    '#title' => t('Upload Product card images'),
    '#default_value' => theme_get_setting('product_image_1'),
    '#upload_location' => 'public://theme/product_image/',
    '#upload_validators' => [
      'file_validate_extensions' => ['jpg jpeg png webp'],
    ],
  ];

  $form['Products']['card_one']['details']['card_title_1'] = [
    '#type'        => 'textfield',
    '#title'       => t('Title'),
    '#default_value' => theme_get_setting('card_title_1'),
    '#description'   => t('')
  ];

  $form['Products']['card_one']['details']['card_price_1'] = [
    '#type'        => 'textfield',
    '#title'       => t('Price'),
    '#default_value' => theme_get_setting('card_price_1'),
    '#description'   => t('')
  ];

  $form['Products']['card_one']['details']['card_link1'] = [
    '#type'        => 'textfield',
    '#title'       => t('Link'),
    '#default_value' => theme_get_setting('card_link1'),
    '#description'   => t('Enter the external link where want to go once click upon the card..')
  ];


  /**
   * cards data  2
   */
  $form['Products']['card_two'] = [
    '#type'        => 'details',
    '#title'       => t('Card Two'),
    '#description'   => t(''),
    '#group' => 'product_tab',
  ];

  $form['Products']['card_two']['details'] = [
    '#type'        => 'fieldset',
    '#title'       => t('Card Details'),
    '#description'   => t('')
  ];

  $form['Products']['card_two']['details']['product_image_2'] = [
    '#type' => 'managed_file',
    '#title' => t('Upload Product card images 2'),
    '#default_value' => theme_get_setting('product_image_2'),
    '#upload_location' => 'public://theme/product_image/',
    '#upload_validators' => [
      'file_validate_extensions' => ['jpg jpeg png webp'],
    ],
  ];

  $form['Products']['card_two']['details']['card_title_2'] = [
    '#type'        => 'textfield',
    '#title'       => t('Title'),
    '#default_value' => theme_get_setting('card_title_2'),
    '#description'   => t('')
  ];

  $form['Products']['card_two']['details']['card_price_2'] = [
    '#type'        => 'textfield',
    '#title'       => t('Price'),
    '#default_value' => theme_get_setting('card_title_2'),
    '#description'   => t('')
  ];

  $form['Products']['card_two']['details']['card_link2'] = [
    '#type'        => 'textfield',
    '#title'       => t('Link'),
    '#default_value' => theme_get_setting('card_link2'),
    '#description'   => t('Enter the external link where want to go once click upon the card..')
  ];


  /**
   * cards data 3
   */
  $form['Products']['card_three'] = [
    '#type'  => 'details',
    '#title' => t('Card Three'),
    '#description'   => t(''),
    '#group' => 'product_tab',
  ];

  $form['Products']['card_three']['details'] = [
    '#type'        => 'fieldset',
    '#title'       => t('Card Details'),
    '#description'   => t('')
  ];

  $form['Products']['card_three']['details']['product_image_3'] = [
    '#type' => 'managed_file',
    '#title' => t('Upload Product card images'),
    '#default_value' => theme_get_setting('product_image_3'),
    '#upload_location' => 'public://theme/product_image/',
    '#upload_validators' => [
      'file_validate_extensions' => ['jpg jpeg png webp'],
    ],
  ];

  $form['Products']['card_three']['details']['card_title_3'] = [
    '#type'        => 'textfield',
    '#title'       => t('Product Title 3'),
    '#default_value' => theme_get_setting('card_title_3'),
    '#description'   => t('')
  ];

  $form['Products']['card_three']['details']['card_price_3'] = [
    '#type'        => 'textfield',
    '#title'       => t('Product Price 3'),
    '#default_value' => theme_get_setting('card_price_3'),
    '#description'   => t('')
  ];

  $form['Products']['card_three']['details']['card_link3'] = [
    '#type'        => 'textfield',
    '#title'       => t('Link'),
    '#default_value' => theme_get_setting('card_link3'),
    '#description'   => t('Enter the external link where want to go once click upon the card..')
  ];

  /**
   * About Us Section
   */
  $form['About']['aboutus_image'] = [
    '#type' => 'managed_file',
    '#title' => t('Upload About Us section images'),
    '#default_value' => theme_get_setting('aboutus_image'),
    '#upload_location' => 'public://theme/about/',
    '#upload_validators' => [
      'file_validate_extensions' => ['jpg jpeg png gif'],
    ],
  ];

  $form['About']['aboutus_heading'] = [
    '#type'        => 'textfield',
    '#title'       => t('Heading'),
    '#default_value' => theme_get_setting('aboutus_heading'),
    '#description'   => t('Here you can enter the heading for about us section.')
  ];

  $form['About']['aboutus_desc'] = [
    '#type'        => 'textarea',
    '#title'       => t('Description'),
    '#default_value' => theme_get_setting('aboutus_desc'),
    '#description'   => t('Here you can enter the Description for about us section.')
  ];

  /**
   * Video Banner Section
   */
  $form['video_banner']['video_banner_image'] = [
    '#type' => 'managed_file',
    '#title' => t('Upload Video Banner section images 1'),
    '#default_value' => theme_get_setting('video_banner_image'),
    '#description'   => t('Here you can upload image for Video banner section. format - > jpg jpeg png gif'),
    '#upload_location' => 'public://theme/video_banner/',
    '#upload_validators' => [
      'file_validate_extensions' => ['jpg jpeg png gif'],
    ],
  ];

  $form['video_banner']['videobanner_heading'] = [
    '#type'        => 'textfield',
    '#title'       => t('Heading'),
    '#default_value' => theme_get_setting('videobanner_heading'),
    '#description'   => t('Here you can enter the heading for Video banner section.')
  ];

  $form['video_banner']['videobanner_subheading'] = [
    '#type'        => 'textfield',
    '#title'       => t('Sub heading'),
    '#default_value' => theme_get_setting('videobanner_subheading'),
    '#description'   => t('Here you can enter the Sub heading for Video banner section.')
  ];

  $form['video_banner']['videobanner_desc'] = [
    '#type'        => 'textarea',
    '#title'       => t('Description'),
    '#default_value' => theme_get_setting('videobanner_desc'),
    '#description'   => t('Here you can enter the Description for Video banner section.')
  ];

  $form['video_banner']['videobanner_btnlink'] = [
    '#type'        => 'fieldset',
    '#title'       => t('Button Detail'),
    '#description'   => t('')
  ];

  $form['video_banner']['videobanner_btnlink']['videobanner_btntext'] = [
    '#type'        => 'textfield',
    '#title'       => t('Button Text'),
    '#default_value' => theme_get_setting('videobanner_btntext'),
    '#description'   => t('Here you can enter the Button Text.')
  ];

  $form['video_banner']['videobanner_btnlink']['videobanner_btnurl'] = [
    '#type'        => 'textfield',
    '#title'       => t('Button URL'),
    '#default_value' => theme_get_setting('videobanner_btnurl'),
    '#description'   => t('Here you can enter the Button URL.')
  ];

  $form['video_banner']['video_link'] = [
    '#type'        => 'fieldset',
    '#title'       => t('Please ensure that you enter the video in only one field, either as an MP4 file or as an external source. '),
    '#description'   => t('')
  ];

  $form['video_banner']['video_link']['video_banner_video'] = [
    '#type' => 'managed_file',
    '#title' => t('Upload Video Banner section video'),
    '#default_value' => theme_get_setting('video_banner_video'),
    '#description'   => t('Here you can upload video for Video banner section.'),
    '#upload_location' => 'public://theme/video_banner/',
    '#upload_validators' => [
      'file_validate_extensions' => ['mp4'],
    ],
  ];

  $form['video_banner']['video_link']['videobanner_youtube'] = [
    '#type'        => 'textfield',
    '#title'       => t('Video Link'),
    '#default_value' => theme_get_setting('videobanner_youtube'),
    '#description'   => t('Here you can enter the video link for Video banner section.')
  ];

  /**
   * Testimonial data  1
   */
  $form['testimonial']['testimonial_card_one'] = [
    '#type'        => 'details',
    '#title'       => t('Testimonial one'),
    '#description'   => t(''),
    '#group' => 'testimonial_tab',
  ];

  $form['testimonial']['testimonial_card_one']['details'] = [
    '#type'        => 'fieldset',
    '#title'       => t('Testimonial Details'),
    '#description'   => t('')
  ];

  $form['testimonial']['testimonial_card_one']['details']['testimonial_title1'] = [
    '#type'        => 'textfield',
    '#title'       => t('Title'),
    '#default_value' => theme_get_setting('testimonial_title1'),
    '#description'   => t('')
  ];

  $form['testimonial']['testimonial_card_one']['details']['testimonial_desc1'] = [
    '#type'        => 'textarea',
    '#title'       => t('Description'),
    '#default_value' => theme_get_setting('testimonial_desc1'),
    '#description'   => t('')
  ];

  $form['testimonial']['testimonial_card_one']['details']['testimonial_author_name1'] = [
    '#type'        => 'textfield',
    '#title'       => t('Author Name'),
    '#default_value' => theme_get_setting('testimonial_author_name1'),
    '#description'   => t('')
  ];

  $form['testimonial']['testimonial_card_one']['details']['testimonial_position_name1'] = [
    '#type'        => 'textfield',
    '#title'       => t('Position Name'),
    '#default_value' => theme_get_setting('testimonial_position_name1'),
    '#description'   => t('')
  ];

  /**
   * Testimonial data  2
   */
  $form['testimonial']['testimonial_card_two'] = [
    '#type'        => 'details',
    '#title'       => t('Testimonial Two'),
    '#description'   => t(''),
    '#group' => 'testimonial_tab',
  ];

  $form['testimonial']['testimonial_card_two']['details'] = [
    '#type'        => 'fieldset',
    '#title'       => t('Testimonial Details'),
    '#description'   => t('')
  ];

  $form['testimonial']['testimonial_card_two']['details']['testimonial_title2'] = [
    '#type'        => 'textfield',
    '#title'       => t('Title'),
    '#default_value' => theme_get_setting('testimonial_title2'),
    '#description'   => t('')
  ];

  $form['testimonial']['testimonial_card_two']['details']['testimonial_desc2'] = [
    '#type'        => 'textarea',
    '#title'       => t('Description'),
    '#default_value' => theme_get_setting('testimonial_desc2'),
    '#description'   => t('')
  ];

  $form['testimonial']['testimonial_card_two']['details']['testimonial_author_name2'] = [
    '#type'        => 'textfield',
    '#title'       => t('Author Name'),
    '#default_value' => theme_get_setting('testimonial_author_name2'),
    '#description'   => t('')
  ];

  $form['testimonial']['testimonial_card_two']['details']['testimonial_position_name2'] = [
    '#type'        => 'textfield',
    '#title'       => t('Position Name'),
    '#default_value' => theme_get_setting('testimonial_position_name2'),
    '#description'   => t('')
  ];

  /**
   * Testimonial data  3
   */
  $form['testimonial']['testimonial_card_three'] = [
    '#type'        => 'details',
    '#title'       => t('Testimonial Three'),
    '#description'   => t(''),
    '#group' => 'testimonial_tab',
  ];

  $form['testimonial']['testimonial_card_three']['details'] = [
    '#type'        => 'fieldset',
    '#title'       => t('Testimonial Details'),
    '#description'   => t('')
  ];

  $form['testimonial']['testimonial_card_three']['details']['testimonial_title3'] = [
    '#type'        => 'textfield',
    '#title'       => t('Title'),
    '#default_value' => theme_get_setting('testimonial_title3'),
    '#description'   => t('')
  ];

  $form['testimonial']['testimonial_card_three']['details']['testimonial_desc3'] = [
    '#type'        => 'textarea',
    '#title'       => t('Description'),
    '#default_value' => theme_get_setting('testimonial_desc3'),
    '#description'   => t('')
  ];

  $form['testimonial']['testimonial_card_three']['details']['testimonial_author_name3'] = [
    '#type'        => 'textfield',
    '#title'       => t('Author Name'),
    '#default_value' => theme_get_setting('testimonial_author_name3'),
    '#description'   => t('')
  ];

  $form['testimonial']['testimonial_card_three']['details']['testimonial_position_name3'] = [
    '#type'        => 'textfield',
    '#title'       => t('Position Name'),
    '#default_value' => theme_get_setting('testimonial_position_name3'),
    '#description'   => t('')
  ];

  /**
   * Testimonial data  4
   */
  $form['testimonial']['testimonial_card_four'] = [
    '#type'        => 'details',
    '#title'       => t('Testimonial Four'),
    '#description'   => t(''),
    '#group' => 'testimonial_tab',
  ];

  $form['testimonial']['testimonial_card_four']['details'] = [
    '#type'        => 'fieldset',
    '#title'       => t('Testimonial Details'),
    '#description'   => t('')
  ];

  $form['testimonial']['testimonial_card_four']['details']['testimonial_title4'] = [
    '#type'        => 'textfield',
    '#title'       => t('Title'),
    '#default_value' => theme_get_setting('testimonial_title4'),
    '#description'   => t('')
  ];

  $form['testimonial']['testimonial_card_four']['details']['testimonial_desc4'] = [
    '#type'        => 'textarea',
    '#title'       => t('Description'),
    '#default_value' => theme_get_setting('testimonial_desc4'),
    '#description'   => t('')
  ];

  $form['testimonial']['testimonial_card_four']['details']['testimonial_author_name4'] = [
    '#type'        => 'textfield',
    '#title'       => t('Author Name'),
    '#default_value' => theme_get_setting('testimonial_author_name4'),
    '#description'   => t('')
  ];

  $form['testimonial']['testimonial_card_four']['details']['testimonial_position_name4'] = [
    '#type'        => 'textfield',
    '#title'       => t('Position Name'),
    '#default_value' => theme_get_setting('testimonial_position_name4'),
    '#description'   => t('')
  ];

  /**
   * Blogs Section
   */
  $form['blog']['blog_banner_image'] = [
    '#type' => 'managed_file',
    '#title' => t('Upload images for Blogs'),
    '#default_value' => theme_get_setting('blog_banner_image'),
    '#description'   => t(''),
    '#upload_location' => 'public://theme/blog/',
    '#upload_validators' => [
      'file_validate_extensions' => ['jpg jpeg png webp'],
    ],
  ];

  $form['blog']['blog_heading'] = [
    '#type'        => 'textfield',
    '#title'       => t('Heading'),
    '#default_value' => theme_get_setting('blog_heading'),
    '#description'   => t('')
  ];

  $form['blog']['blog_subheading'] = [
    '#type'        => 'textfield',
    '#title'       => t('Sub heading'),
    '#default_value' => theme_get_setting('blog_subheading'),
    '#description'   => t('')
  ];

  $form['blog']['blog_desc'] = [
    '#type'        => 'textarea',
    '#title'       => t('Description'),
    '#default_value' => theme_get_setting('blog_desc'),
    '#description'   => t('')
  ];

  /**
   * Footer Logo Section
   */
  $form['footer_logo']['footer_logo1'] = [
    '#type' => 'managed_file',
    '#title' => t('Upload images for footer logo'),
    '#default_value' => theme_get_setting('footer_logo1'),
    '#description'   => t(''),
    '#upload_location' => 'public://theme/footer-logo/',
    '#upload_validators' => [
      'file_validate_extensions' => ['jpg jpeg png webp'],
    ],
  ];

  $form['footer_logo']['footer_logo2'] = [
    '#type' => 'managed_file',
    '#title' => t('Upload images for footer logo'),
    '#default_value' => theme_get_setting('footer_logo2'),
    '#description'   => t(''),
    '#upload_location' => 'public://theme/footer-logo/',
    '#upload_validators' => [
      'file_validate_extensions' => ['jpg jpeg png webp'],
    ],
  ];

  $form['footer_logo']['footer_logo3'] = [
    '#type' => 'managed_file',
    '#title' => t('Upload images for footer logo'),
    '#default_value' => theme_get_setting('footer_logo3'),
    '#description'   => t(''),
    '#upload_location' => 'public://theme/footer-logo/',
    '#upload_validators' => [
      'file_validate_extensions' => ['jpg jpeg png webp'],
    ],
  ];

  $form['footer_logo']['footer_logo4'] = [
    '#type' => 'managed_file',
    '#title' => t('Upload images for footer logo'),
    '#default_value' => theme_get_setting('footer_logo4'),
    '#description'   => t(''),
    '#upload_location' => 'public://theme/footer-logo/',
    '#upload_validators' => [
      'file_validate_extensions' => ['jpg jpeg png webp'],
    ],
  ];

  $form['footer_logo']['footer_logo5'] = [
    '#type' => 'managed_file',
    '#title' => t('Upload images for footer logo'),
    '#default_value' => theme_get_setting('footer_logo5'),
    '#description'   => t(''),
    '#upload_location' => 'public://theme/footer-logo/',
    '#upload_validators' => [
      'file_validate_extensions' => ['jpg jpeg png webp'],
    ],
  ];
  
  /**
   * Footer Section
   */
  $form['footer_description']['footer_desc'] = [
    '#type'        => 'textarea',
    '#title'       => t(' Footer Description'),
    '#default_value' => theme_get_setting('footer_desc'),
    '#description'   => t('')
  ];
}

(function ($, Drupal) {
  "use strict";
  Drupal.behaviors.global = {
    attach: function (context, settings) {
      $(document).ready(function () {
        console.log("hello");
        $(".menu-bar").click(function () {
          $(".mobile_view").addClass("active");
        });

        $(".mobile_close").click(function () {
          $(".mobile_view").removeClass("active");
        });

        $("#testimonial-slider").owlCarousel({
          loop: false,
          rewind: true,
          dots: true,
          margin: 0,
          responsive: {
            0: {
              items: 1,
            },
            479: {
              items: 1,
            },
            768: {
              items: 1,
            },
            979: {
              items: 1,
            },
            1199: {
              items: 1,
            },
          },
        });

        Fancybox.bind("[data-fancybox]", {
          // Your custom options
          //if you want to customize the setting
        });
      });
    },
  };
})(jQuery, Drupal);
